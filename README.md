# To do do

> Local storage powered lame to-do list.

## Running

```sh
yarn dev
```

Or [try it out here](https://tododo.hubelbauer.net).

## Building

`PORT=1235 parcel build src/index.html --public-url .`

## Deploying

```bash
yarn run check
wsl
git push dokku master
```
