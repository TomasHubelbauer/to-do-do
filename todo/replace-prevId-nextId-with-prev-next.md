# Replace `prevId` and `nextId` on items with `prev` and `next`

This way we don't need to pass `items` to `renderItem` and we can simplify move up and move down logic to get rid of the lookups.
