import recurse from 'net-tree';

window.addEventListener('load', _ => {
  let searchFilter;
  let showArchived = false;

  const draftInput = document.querySelector<HTMLInputElement>('#draftInput');
  const queryInput = document.querySelector<HTMLInputElement>('#queryInput');
  const itemsUl = document.querySelector<HTMLUListElement>('#itemsUl');
  const showArchivedInput = document.querySelector<HTMLInputElement>('#showArchivedInput');
  const deleteAllButton = document.querySelector<HTMLButtonElement>('#deleteAllButton');
  const exportAllButton = document.querySelector<HTMLButtonElement>('#exportAllButton');
  const weekNumberSpan = document.querySelector<HTMLSpanElement>('#weekNumberSpan');

  draftInput.focus();
  draftInput.value = '';

  document.body.addEventListener('keydown', event => {
    if (event.ctrlKey) {
      switch (event.key) {
        case 'r': {
          event.preventDefault();
          location.reload();
          return;
        }
        case 'd': {
          event.preventDefault();
          draftInput.focus();
          return;
        }
        case 'f': {
          event.preventDefault();
          queryInput.focus();
          return;
        }
      }
    }
  });

  document.addEventListener('visibilitychange', _ => {
    draftInput.focus();
  });

  draftInput.addEventListener('keydown', ({ key }) => {
    if (key === 'Enter' && draftInput.value.length > 0) {
      let priorKey;
      for (let index = 0; index < localStorage.length; index++) {
        const key = Number(localStorage.key(index));
        if (priorKey === undefined || priorKey < key) {
          priorKey = key;
        }
      }

      localStorage.setItem((priorKey || 0) + 1, JSON.stringify({ title: draftInput.value, priorIds: priorKey ? { undefined: priorKey }: undefined }));
      draftInput.value = '';
      render();
    }
  });

  queryInput.addEventListener('input', _ => {
    searchFilter = queryInput.value;
    render();
  });

  showArchivedInput.addEventListener('change', _ => {
    showArchived = showArchivedInput.checked;
    render();
  });

  deleteAllButton.addEventListener('click', _ => {
    if (confirm('Are you sure you want to irreversibly delete all the items?')) {
      localStorage.clear();
      render();
    }
  });

  exportAllButton.addEventListener('click', _ => {
    const timestamp = (new Date()).toISOString().replace('T', '-').replace(/:/g, '-');
    const downloadA = document.createElement('a');
    downloadA.download = `${timestamp}-to-do-do-export.json`;
    downloadA.href = 'data:application/json;charset=utf8,' + encodeURIComponent(JSON.stringify({ ...localStorage, timestamp: new Date() }, null, 2));
    document.body.appendChild(downloadA);
    downloadA.click();
    document.body.removeChild(downloadA);
  });

  render();

  function render() {
    const date = new Date();
    date.setUTCDate(date.getUTCDate() + 4 - (date.getUTCDay() || 7));
    weekNumberSpan.textContent = Math.ceil((((date.getTime() - new Date(Date.UTC(date.getUTCFullYear(), 0, 1)).getTime()) / 86400000) + 1) / 7).toString();

    let items = [];
    for (let index = 0; index < localStorage.length; index++) {
      const id = Number(localStorage.key(index));
      const data = JSON.parse(localStorage.getItem(id as any));
      const item = { id, priorIds: data.priorIds, data };
      if (!searchFilter || (item.data.title.includes(searchFilter) || (item.data.content && item.data.content.includes(searchFilter)))) {
        items.push(item);
      }
    }

    itemsUl.innerHTML = '';

    let archived = 0;
    let crossed = 0;
    let uncrossed = 0;

    // Keep 2 items back and process 1 item back so we can always calculate `prevId` and `nextId`
    let pivot;
    let prior;
    for (const { item } of recurse<any>(items, {})) {
      if (item.data.isArchived) {
        archived++;
      } else {
        if (item.data.isCrossed) {
          crossed++;
        } else {
          uncrossed++;
        }
      }
    
      if (item.data.isArchived && !showArchived) {
        continue;
      }

      if (pivot) {
        pivot.prevId = prior ? prior.id : undefined;
        pivot.nextId = item.id;
        itemsUl.appendChild(renderItem(pivot, items));
      }

      prior = pivot;
      pivot = item;
    }

    if (pivot) {
      pivot.prevId = prior ? prior.id : undefined;
      pivot.nextId = undefined;
      itemsUl.appendChild(renderItem(pivot, items));
    }

    document.title = `${uncrossed} | ${crossed} | ${archived}`;

    document.querySelector('link[rel=icon]').remove();

    const iconCanvas = document.createElement('canvas');
    iconCanvas.width = 16;
    iconCanvas.height = 16;

    const context = iconCanvas.getContext('2d');
    context.font = 'icon';
    context.lineWidth = 3;
    context.textAlign = 'center';
    context.strokeText(uncrossed.toString(), 8, 12);
    context.fillStyle = 'white';
    context.fillText(uncrossed.toString(), 8, 12);

    const iconLink = document.createElement('link');
    iconLink.rel = 'icon';
    iconLink.href = iconCanvas.toDataURL();

    // TODO: Fix icon not rendering on first load
    document.head.appendChild(iconLink);
  }

  function renderItem(item, items) {
    const itemLi = document.createElement('li');
  
    if (item.data.isArchived) {
      // TODO: Still display and unarchive if uncrossed, keep archived if crossed?
    } else {
      const checkboxInput = document.createElement('input');
      itemLi.appendChild(checkboxInput);
  
      checkboxInput.type = 'checkbox';
      checkboxInput.checked = item.data.isCrossed;
  
      checkboxInput.addEventListener('change', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, isCrossed: checkboxInput.checked }));
        render();
      });
    }
  
    if (item.data.isEditing) {
      const editorDiv = document.createElement('div');
      editorDiv.className = 'editor';
  
      const titleInput = document.createElement('input');
      editorDiv.appendChild(titleInput);
  
      const contentTextArea = document.createElement('textarea');
      editorDiv.appendChild(contentTextArea);
  
      titleInput.id = 'titleEditorInput' + item.id;
      titleInput.value = item.data.title;
      titleInput.className = item.data.isCrossed ? 'crossed' : '';
  
      titleInput.addEventListener('input', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, title: titleInput.value }));
      });
  
      titleInput.addEventListener('keydown', ({ key }) => {
        if (key === 'Enter') {
          localStorage.setItem(item.id, JSON.stringify({ ...item.data, title: titleInput.value, isEditing: false }));
          render();
        }
      });
  
      titleInput.addEventListener('blur', ({ relatedTarget }) => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, title: titleInput.value, isEditing: false }));
        if (relatedTarget !== contentTextArea) {
          render();
        }
      });
  
      contentTextArea.id = 'contentEditorTextArea' + item.id;
      contentTextArea.value = item.data.content || '';
  
      contentTextArea.addEventListener('input', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, content: contentTextArea.value }));
      });
  
      contentTextArea.addEventListener('keydown', ({ key, ctrlKey }) => {
        if (key === 'Enter' && ctrlKey) {
          localStorage.setItem(item.id, JSON.stringify({ ...item.data, content: contentTextArea.value, isEditing: false }));
          render();
        }
      });
  
      contentTextArea.addEventListener('blur', ({ relatedTarget }) => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, content: contentTextArea.value, isEditing: false }));
        if (relatedTarget !== titleInput) {
          render();
        }
      });
  
      itemLi.appendChild(editorDiv);
    } else {
      const viewerDiv = document.createElement('div');
      viewerDiv.className = 'viewer';
  
      const titleLabel = document.createElement('label');
      viewerDiv.appendChild(titleLabel);
  
      titleLabel.textContent = item.data.title;
      titleLabel.classList.add('title');
  
      if (item.data.isCrossed) {
        titleLabel.classList.add('crossed');
      }
  
      if (item.data.isArchived) {
        titleLabel.classList.add('archived');
      }
  
      titleLabel.addEventListener('click', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, isEditing: true }));
        render();
        document.getElementById('titleEditorInput' + item.id).focus();
      });
  
      const contentLabel = document.createElement('label');
      viewerDiv.appendChild(contentLabel);
  
      contentLabel.textContent = item.data.content;
      contentLabel.classList.add('content');
  
      if (item.data.isCrossed) {
        contentLabel.classList.add('crossed');
      }
  
      if (item.data.isArchived) {
        contentLabel.classList.add('archived');
      }
  
      contentLabel.addEventListener('click', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, isEditing: true }));
        render();
        document.getElementById('contentEditorTextArea' + item.id).focus();
      });
  
      itemLi.appendChild(viewerDiv);
    }

    if (item.prevId !== undefined) {
      const moveUpButton = document.createElement('button');
      itemLi.appendChild(moveUpButton);
  
      moveUpButton.textContent = '⬆️';

      moveUpButton.addEventListener('click', _ => {
        const prev = items.find(i => i.id === item.prevId);
        localStorage.setItem(prev.id, JSON.stringify({ ...prev.data, priorIds: { undefined: item.id } }));
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, priorIds: { undefined: prev.prevId } }));

        const next = items.find(i => i.id === item.nextId);
        if (next !== undefined) {
          localStorage.setItem(next.id, JSON.stringify({ ...next.data, priorIds: { undefined: item.prevId } }));
        }

        render();
      });
    }
  
    if (item.nextId !== undefined) {
      const moveDownButton = document.createElement('button');
      itemLi.appendChild(moveDownButton);
  
      moveDownButton.textContent = '⬇️';
  
      moveDownButton.addEventListener('click', _ => {
        const next = items.find(i => i.id === item.nextId);
        localStorage.setItem(next.id, JSON.stringify({ ...next.data, priorIds: { undefined: item.prevId } }));
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, priorIds: { undefined: next.id } }));

        const next2 = items.find(i => i.id === next.nextId);;
        if (next2 !== undefined) {
          localStorage.setItem(next2.id, JSON.stringify({ ...next2.data, priorIds: { undefined: item.id } }));
        }

        render();
      });
    }
  
    if (item.data.isArchived) {
      const unarchiveButton = document.createElement('button');
      itemLi.appendChild(unarchiveButton);
  
      unarchiveButton.textContent = '♻️';
  
      unarchiveButton.addEventListener('click', _ => {
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, isArchived: false }));
        render();
      });
  
      const deleteButton = document.createElement('button');
      itemLi.appendChild(deleteButton);
  
      deleteButton.textContent = '🗑️';
  
      deleteButton.addEventListener('click', _ => {
        if (!confirm(`Do you want to delete '${item.data.title}'?`)) {
          return;
        }
  
        localStorage.removeItem(item.id);
        render();
      });
    } else {
      const archiveButton = document.createElement('button');
      itemLi.appendChild(archiveButton);
  
      archiveButton.textContent = '🗑️';
  
      archiveButton.addEventListener('click', _ => {
        let isCrossed = item.data.isCrossed;
        if (!item.data.isCrossed && confirm(`Do you want to cross '${item.data.title}' off?`)) {
          isCrossed = true;
        }
  
        localStorage.setItem(item.id, JSON.stringify({ ...item.data, isArchived: true, isCrossed }));
        render();
      });
    }
  
    return itemLi;
  }
});
